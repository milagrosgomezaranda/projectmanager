﻿using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Clientes
{
    public class Cliente : Entity, IAggregateRoot
    {
        public string NombreCompleto { get; set; }

        public IList<Direccion> Direcciones { get; set; }

        public string Email { get; set; }

        public TipoCliente Tipo { get; set; }

        public IList<Proyecto> Proyectos { get; set; }

        public Cliente(string nombre, Direccion direccion, string email, TipoCliente tipo)
        {
            this.NombreCompleto = nombre;
            AgregarDireccion(direccion);
            this.Email = email;
            this.Tipo = tipo;
        }

        public void AgregarDireccion(Direccion direccion)
        {
            if (this.Direcciones == null)
            {
                this.Direcciones = new List<Direccion>();
            }
            this.Direcciones.Add(direccion);
        }

        public void AgregarProyecto(Proyecto proyecto)
        {
            if (this.Proyectos == null)
            {
                this.Proyectos = new List<Proyecto>();
            }
            this.Proyectos.Add(proyecto);
        }

    }

}
